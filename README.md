# sensor-confirmation

This repository contains a set of scripts and notebooks for confirming sensos routes and linear reference locations.

#### Requirments
~~~~
pip install ipywidget
jupyter nbextension enable --py widgetsnbextension

pip install ipyleaflet
jupyter nbextension enable --py --sys-prefix ipyleaflet 

#if using jupyter lab, need to install these extentions as well
jupyter labextension install @jupyter-widgets/jupyterlab-manager
jupyter labextension install jupyter-leaflet
~~~~


#### Data
Sensor locations and data are stored in ***data/sensor-locations.csv***. Using the app notebook will update this file, so make sure to create a backup of sensor-locations.csv before using notebook,just in case.

#### Scripts
* update-sensor-list.py

Pulls list of sensor inventory and add any new detectors to sensor-list.py

* app.ipynb

Interactive notebook for confirming sesnor route and linear reference measure. Given a selected sensor, user is presented with a list of possible lrs locations as given by the RAMS API. User can select correct route and confirm sensor, updating ***data/sensor-locations.csv***.

