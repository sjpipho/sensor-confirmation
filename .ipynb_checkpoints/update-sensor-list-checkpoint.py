'''
Script to generate list of sensors whose linear reference needs to be confirmed
 '''
import csv
from util import fetch_detectors

sensor_list_file_header = ["Id", "Name", "Confirmed", "Latitude","Longitude", "RouteId", "Measure"]

sensor_ids = set() #set of sensor ids currently in sensor-list.csv

with open("data/sensor-list.csv") as f:
    csv_reader = csv.DictReader(f)
    line_count = 0
    for row in csv_reader:
        line_count += 1
        if line_count == 1:
            continue
        sensor_ids.add(row["Id"])


current_sensors = fetch_detectors() #list of current sensors from DOT API

num_added = 0
#compare current sensors to ones in csv file, append new sensors to csv file
with open("data/sensor-list.csv", "a") as f:
    for sensor in current_sensors:
        if sensor["id"] not in sensor_ids:
            num_added += 1
            data = [sensor["id"], sensor["name"], "False", str(sensor["position"][0]), str(sensor["position"][1]), "None","None"]
            line = ",".join(data) + "\n"
            f.write(line)
            
print("added", num_added, "sensors to sensor-list.csv")