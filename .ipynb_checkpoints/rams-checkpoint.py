import json
import requests

# query rams api and returns potential lrs measures for single location
# location: tuple or list (lat,long)
# tolerance: number (20 is good default)
def query_location(location, tolerance):  
    url = 'https://gis.iowadot.gov/ramsa/rest/services/lrs/MapServer/exts/LRSServer/networkLayers/0/geometryToMeasure?'
    json_location = [{"geometry":{"x":location[1],"y":location[0]}}]
    data = {'f':'json','locations': json.dumps(json_location),'tolerance':str(tolerance),'inSR':'4326'}
    response = requests.post(url, data=data)
    r=response.json()
    
   
    # extract results
    resall=[]
    for i in r['locations']:
        res=[]
        if i['status']==u'esriLocatingCannotFindLocation':
            routeid="0"
            measure=0
            x=0
            y=0
            res.append(None)
        else:
            for j in i['results']:
                routeId=j['routeId']
                measure=j['measure']
                x=j['geometry']['x']
                y=j['geometry']['y']
                res.append({
                    "routeId":routeId,
                    "measure": measure,
                    "latitude":y,
                    "longitude":x
                })
        resall.append(res)
    
    
    return resall[0]