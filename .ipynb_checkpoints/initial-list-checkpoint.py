'''Script to create intial sensor-list.csv using data from IWZ_Sensor_List.csv '''
import csv

sensor_list_file = "sensor_list.csv"
sensor_list_file_header = ["Id", "Name", "Confirmed", "Latitude","Longitude", "RouteId", "Measure"]

rows = []

with open("data/IWZ_Sensor_List.csv") as f:
    csv_reader = csv.DictReader(f)
    line_count = 0
    for row in csv_reader:
        line_count += 1
        if line_count == 1:
            continue
        row_data = [row["NewSensorName"], "name", "True", row["Latitude"], row["Longitude"], row["routeId"], row["Measure"]]
        rows.append(row_data)
        print(row_data)


with open("data/sensor-list.csv", "w") as f:
    f.write(",".join(sensor_list_file_header) + "\n")
    for row in rows:
        f.write(",".join(row) + "\n")
