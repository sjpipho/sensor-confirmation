import json
import requests
from xml.etree import ElementTree

'''
Fetches list of traffic detectors from DOT api
returns [
    {
        "detector-id": "...",
        "detector-name": "...",
        "position":[lat, long]
    },
    ....
]
'''
def fetch_detectors():
    url = "http://205.221.97.102/Iowa.Sims.AllSites.C2C/IADOT_SIMS_AllSites_C2C.asmx/OP_ShareTrafficDetectorInventoryInformation"
    params = {
        "MSG_TrafficDetectorInventoryRequest":"UseLocationReference"
    }
    r = requests.get(url, params=params)

    root = ElementTree.fromstring(r.text)
    for child in root.iter():
         if '}' in child.tag:
            child.tag = child.tag.split('}', 1)[1]
    
    detectors = []
    
    for el in root.iter("detector"):    
        try:
            detectors.append({
                "id": el.find('detector-id').text,
                "name": el.find('detector-name').text,
                "position": [
                    int(el.find("detector-location").find("latitude").text)/1000000,
                    int(el.find("detector-location").find("longitude").text)/1000000
                ]
            })
        except:
            pass
    
    return detectors


def fetch_cameras():
    url = "http://205.221.97.102/Iowa.Sims.AllSites.C2C/IADOT_SIMS_AllSites_C2C.asmx/OP_ShareCCTVInventoryInformation"
    params = {
        "MSG_TrafficDetectorInventoryRequest":"UseLocationReference"
    }
    r = requests.get(url, params=params)
    root = ElementTree.fromstring(r.text)

   
    for child in root.iter():
         if '}' in child.tag:
            child.tag = child.tag.split('}', 1)[1]
    
    cameras = []
    
    for el in root.iter("cctv-inventory-item"):    
        try:
            cameras.append({
                "id": el.find('device-inventory-header').find('device-name').text,
                "position": [
                    int(el.find('device-inventory-header').find('device-location').find('latitude').text)/1000000,
                    int(el.find('device-inventory-header').find('device-location').find("longitude").text)/1000000
                ]
            })
        except:
            pass
    
    return cameras

